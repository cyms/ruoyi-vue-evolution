package com.evolution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Class: EvolutionApplication
 * <p>
 * desc: 启动类
 * </p>
 * create date: 2024-03-05 12:59:27
 *
 * @author cao.yong
 */
@SpringBootApplication
public class EvolutionApplication {
    public static void main(String[] args) {
        SpringApplication.run(EvolutionApplication.class, args);
    }
}
