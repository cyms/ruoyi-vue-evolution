package com.evolution.common.tenant.dataobject;

import com.evolution.common.mybatis.dataobject.BaseDO;
import com.mybatisflex.annotation.Column;

/**
 * Class: TenantDO
 * <p>
 * desc: 租户基类
 * </p>
 * create date: 2024-03-13 12:57:02
 *
 * @author cao.yong
 */
public class TenantDO extends BaseDO {

    @Column(tenantId = true)
    private Long tenantId;

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }
}
