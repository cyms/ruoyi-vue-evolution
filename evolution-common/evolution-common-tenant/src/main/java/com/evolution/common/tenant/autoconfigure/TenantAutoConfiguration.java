package com.evolution.common.tenant.autoconfigure;

import cn.dev33.satoken.dao.SaTokenDao;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import com.evolution.common.mybatis.autoconfigure.MyBatisFlexExtendAutoConfiguration;
import com.evolution.common.redis.autoconfigure.RedissonExtendAutoConfiguration;
import com.evolution.common.redis.autoconfigure.properties.RedissonExtendProperties;
import com.evolution.common.tenant.autoconfigure.properties.TenantProperties;
import com.evolution.common.tenant.dao.TenantSaTokenDao;
import com.evolution.common.tenant.factory.TenantPlusFactory;
import com.evolution.common.tenant.handler.TenantKeyPrefixHandler;
import com.evolution.common.tenant.manager.TenantSpringCacheManager;
import com.mybatisflex.core.tenant.TenantFactory;
import org.redisson.spring.starter.RedissonAutoConfigurationCustomizer;
import org.redisson.config.SingleServerConfig;
import org.redisson.config.ClusterServersConfig;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

/**
 * Class: TenantAutoConfiguration
 * <p>
 * desc: 租户配置类
 * </p>
 * create date: 2024-03-13 13:06:49
 *
 * @author cao.yong
 */
@AutoConfiguration(after = {RedissonExtendAutoConfiguration.class, MyBatisFlexExtendAutoConfiguration.class})
@EnableConfigurationProperties(TenantProperties.class)
@ConditionalOnProperty(prefix = TenantProperties.PREFIX, name = "enabled", havingValue = "true")
public class TenantAutoConfiguration {

    @Bean
    public TenantFactory tenantFactory() {
        return new TenantPlusFactory();
    }

    /**
     * 多租户缓存管理器
     */
    @Primary
    @Bean
    public CacheManager tenantCacheManager() {
        return new TenantSpringCacheManager();
    }

    /**
     * 多租户鉴权dao实现
     */
    @Primary
    @Bean
    public SaTokenDao tenantSaTokenDao() {
        return new TenantSaTokenDao();
    }

    @Bean
    public RedissonAutoConfigurationCustomizer tenantRedissonCustomizer(RedissonExtendProperties redissonProperties) {
        return config -> {
            TenantKeyPrefixHandler nameMapper = new TenantKeyPrefixHandler(redissonProperties.getKeyPrefix());
            SingleServerConfig singleServerConfig = (SingleServerConfig) ReflectUtil.getFieldValue(config, "singleServerConfig");
            if (ObjectUtil.isNotNull(singleServerConfig)) {
                // 使用单机模式
                // 设置多租户 redis key前缀
                singleServerConfig.setNameMapper(nameMapper);
                ReflectUtil.setFieldValue(config, "singleServerConfig", singleServerConfig);
            }
            ClusterServersConfig clusterServersConfig = (ClusterServersConfig) ReflectUtil.getFieldValue(config, "clusterServersConfig");
            // 集群配置方式 参考下方注释
            if (ObjectUtil.isNotNull(clusterServersConfig)) {
                // 设置多租户 redis key前缀
                clusterServersConfig.setNameMapper(nameMapper);
                ReflectUtil.setFieldValue(config, "clusterServersConfig", clusterServersConfig);
            }
        };
    }
}
