package com.evolution.common.tenant.handler;

import cn.hutool.core.util.ObjectUtil;
import com.evolution.common.core.constant.GlobalConstant;
import com.evolution.common.core.utils.StringUtils;
import com.evolution.common.redis.handler.KeyPrefixHandler;
import com.evolution.common.tenant.utils.TenantUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class: TenantKeyPrefixHandler
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-13 13:25:26
 *
 * @author cao.yong
 */
public class TenantKeyPrefixHandler extends KeyPrefixHandler {

    private static final Logger log = LoggerFactory.getLogger(TenantKeyPrefixHandler.class);

    public TenantKeyPrefixHandler(String keyPrefix) {
        super(keyPrefix);
    }

    /**
     * 增加前缀
     */
    @Override
    public String map(String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
        if (StringUtils.contains(name, GlobalConstant.GLOBAL_REDIS_KEY)) {
            return super.map(name);
        }
        Long tenantId = TenantUtils.getTenantId();
        if (ObjectUtil.isNull(tenantId)) {
            log.error("无法获取有效的租户id -> Null");
        }
        if (StringUtils.startWith(name, tenantId + "")) {
            // 如果存在则直接返回
            return super.map(name);
        }
        return super.map(tenantId + ":" + name);
    }

    /**
     * 去除前缀
     */
    @Override
    public String unmap(String name) {
        String unmap = super.unmap(name);
        if (StringUtils.isBlank(unmap)) {
            return null;
        }
        if (StringUtils.contains(name, GlobalConstant.GLOBAL_REDIS_KEY)) {
            return super.unmap(name);
        }
        Long tenantId = TenantUtils.getTenantId();
        if (StringUtils.startWith(unmap, tenantId + "")) {
            // 如果存在则删除
            return unmap.substring((tenantId + ":").length());
        }
        return unmap;
    }
}
