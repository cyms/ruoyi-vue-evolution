package com.evolution.common.tenant.factory;

import cn.hutool.core.util.ObjectUtil;
import com.evolution.common.security.context.LoginContext;
import com.evolution.common.tenant.utils.TenantUtils;
import com.mybatisflex.core.tenant.TenantFactory;

/**
 * Class: TenantPlusFactory
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-13 13:13:40
 *
 * @author cao.yong
 */
public class TenantPlusFactory implements TenantFactory {
    @Override
    public Object[] getTenantIds() {
        Long tenantId = LoginContext.getTenantId();
        if (ObjectUtil.isNull(tenantId)) {
            return null;
        }
        Long dynamicTenantId = TenantUtils.getDynamic();
        if (ObjectUtil.isNotNull(dynamicTenantId)) {
            // 返回动态租户
            return new Object[]{dynamicTenantId};
        }
        // 返回固定租户
        return new Object[]{tenantId};
    }
}
