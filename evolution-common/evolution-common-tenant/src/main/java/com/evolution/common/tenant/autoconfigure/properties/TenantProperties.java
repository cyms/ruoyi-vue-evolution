package com.evolution.common.tenant.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * Class: TenantProperties
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-13 13:07:11
 *
 * @author cao.yong
 */
@ConfigurationProperties(TenantProperties.PREFIX)
public class TenantProperties {
    public static final String PREFIX = "evolution.tenant";

    /**
     * 是否启用
     */
    private Boolean enabled;

    /**
     * 排除表
     */
    private List<String> excludes;

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<String> getExcludes() {
        return excludes;
    }

    public void setExcludes(List<String> excludes) {
        this.excludes = excludes;
    }
}
