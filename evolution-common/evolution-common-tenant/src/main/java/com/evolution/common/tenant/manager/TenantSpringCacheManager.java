package com.evolution.common.tenant.manager;

import com.evolution.common.core.constant.GlobalConstant;
import com.evolution.common.core.utils.StringUtils;
import com.evolution.common.redis.manager.SpringCachePlusManager;
import com.evolution.common.tenant.utils.TenantUtils;
import org.springframework.cache.Cache;
import org.springframework.lang.NonNull;

/**
 * Class: TenantSpringCacheManager
 * <p>
 * desc: 重写 cacheName 处理方法 支持多租户
 * </p>
 * create date: 2024-03-13 13:21:03
 *
 * @author cao.yong
 */
public class TenantSpringCacheManager extends SpringCachePlusManager {

    public TenantSpringCacheManager() {
    }

    @Override
    public Cache getCache(@NonNull String name) {
        if (StringUtils.contains(name, GlobalConstant.GLOBAL_REDIS_KEY)) {
            return super.getCache(name);
        }
        Long tenantId = TenantUtils.getTenantId();
        if (StringUtils.startWith(name, tenantId + "")) {
            // 如果存在则直接返回
            return super.getCache(name);
        }
        return super.getCache(tenantId + ":" + name);
    }
}
