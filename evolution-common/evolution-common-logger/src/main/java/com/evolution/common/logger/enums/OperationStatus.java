package com.evolution.common.logger.enums;

/**
 * Class: OperationStatus
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-13 16:05:37
 *
 * @author cao.yong
 */
public enum OperationStatus {
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAILURE,
}
