package com.evolution.common.logger.annotation;

import com.evolution.common.logger.enums.OperationType;
import com.evolution.common.logger.enums.OperatorType;

import java.lang.annotation.*;

/**
 * Class: OperateLogger
 * <p>
 * desc: 日志记录器
 * </p>
 * create date: 2024-03-13 15:35:38
 *
 * @author cao.yong
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OperateLogger {
    /**
     * 功能名称
     */
    String name() default "";

    /**
     * 操作类型
     */
    OperationType operationType() default OperationType.OTHER;

    /**
     * 操作人类别
     */
    OperatorType operatorType() default OperatorType.MANAGE;

    /**
     * 是否保存请求的参数
     */
    boolean isSaveRequest() default true;

    /**
     * 是否保存响应的参数
     */
    boolean isSaveResponse() default true;

    /**
     * 排除指定的请求参数
     */
    String[] excludeParams() default {};
}
