package com.evolution.common.logger.enums;

/**
 * Class: OperationType
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-13 15:40:15
 *
 * @author cao.yong
 */
public enum OperationType {
    /**
     * 其它
     */
    OTHER,

    /**
     * 创建
     */
    CREATE,

    /**
     * 修改
     */
    UPDATE,

    /**
     * 删除
     */
    DELETE,

    /**
     * 授权
     */
    GRANT,

    /**
     * 导出
     */
    EXPORT,

    /**
     * 导入
     */
    IMPORT,

    /**
     * 强退
     */
    FORCE,

    /**
     * 生成代码
     */
    GENERATE_CODE,

    /**
     * 清空数据
     */
    CLEAN,
}
