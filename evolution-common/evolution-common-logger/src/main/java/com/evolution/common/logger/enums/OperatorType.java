package com.evolution.common.logger.enums;

/**
 * Class: OperatorType
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-13 15:41:44
 *
 * @author cao.yong
 */
public enum OperatorType {
    /**
     * 其它
     */
    OTHER,

    /**
     * 后台用户
     */
    MANAGE,

    /**
     * 手机端用户
     */
    MOBILE
}
