package com.evolution.common.logger.autoconfigure;

import com.evolution.common.logger.aspect.LoggerAspect;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * Class: LoggerAutoConfiguration
 * <p>
 * desc: 日志记录器 自动配置
 * </p>
 * create date: 2024-03-13 16:34:06
 *
 * @author cao.yong
 */
@AutoConfiguration
public class LoggerAutoConfiguration {

    @Bean
    public LoggerAspect loggerAspect() {
        return new LoggerAspect();
    }
}
