package com.evolution.common.logger.event;

import jakarta.servlet.http.HttpServletRequest;

import java.io.Serial;
import java.io.Serializable;

/**
 * Class: LoginEvent
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-13 16:03:48
 *
 * @author cao.yong
 */
public class LoginEvent implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 用户账号
     */
    private String loginName;

    /**
     * 登录状态 0成功 1失败
     */
    private String status;

    /**
     * 提示消息
     */
    private String message;

    /**
     * 请求体
     */
    private HttpServletRequest request;

    /**
     * 其他参数
     */
    private Object[] args;
}
