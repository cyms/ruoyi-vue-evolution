package com.evolution.common.core.exception;

import java.io.Serial;

/**
 * Class: ServiceException
 * <p>
 * desc: 服务异常
 * </p>
 * create date: 2024-03-07 13:20:57
 *
 * @author cao.yong
 */
public class ServiceException extends BaseException {

    @Serial
    private static final long serialVersionUID = 1L;

    public ServiceException(String reason) {
        super(reason);
    }

    public ServiceException(String module, String reason) {
        super(module, reason);
    }

    public ServiceException(int code, String reason) {
        super(code, reason);
    }

    public ServiceException(int code, String reason, Object... args) {
        super(code, reason, args);
    }

    public ServiceException(String reason, Object... args) {
        super(reason, args);
    }

    public ServiceException(String module, int code, String reason, Object... args) {
        super(module, code, reason, args);
    }
}
