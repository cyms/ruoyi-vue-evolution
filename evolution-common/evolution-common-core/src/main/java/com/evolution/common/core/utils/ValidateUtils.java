package com.evolution.common.core.utils;

import com.evolution.common.core.context.SpringContext;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;

import java.util.Set;

/**
 * Class: ValidateUtils
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-11 15:42:25
 *
 * @author cao.yong
 */
public class ValidateUtils {
    private static final Validator VALID = SpringContext.getBean(Validator.class);

    public static <T> void validate(T object, Class<?>... groups) {
        Set<ConstraintViolation<T>> validate = VALID.validate(object, groups);
        if (!validate.isEmpty()) {
            throw new ConstraintViolationException("参数校验异常", validate);
        }
    }

}
