package com.evolution.common.core.enums;

import com.evolution.common.core.utils.StringUtils;

/**
 * Class: UserType
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-07 18:19:02
 *
 * @author cao.yong
 */
public enum UserType {

    SYS_USER("SYS_USER"),
    APP_USER("APP_USER");

    final String type;

    UserType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static UserType of(String type) {
        for (UserType value : values()) {
            if (StringUtils.contains(type, value.getType())) {
                return value;
            }
        }
        throw new RuntimeException("'UserType' not found By " + type);
    }
}
