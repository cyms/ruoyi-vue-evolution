package com.evolution.common.core.enums;

/**
 * Class: LoginLimitType
 * <p>
 * desc: 登录限制类型
 * </p>
 * create date: 2024-03-06 15:59:06
 *
 * @author cao.yong
 */
public enum LoginLimitType {

    PASSWORD("password.retry.exceed.limit", "password.retry.count.limit"),
    SMS("sms.retry.exceed.limit", "sms.retry.count.limit"),
    EMAIL("email.retry.exceed.limit", "email.retry.count.limit"),
    MINI("mini.retry.exceed.limit", "mini.retry.count.limit");

    /**
     * 重试超出限制提示
     */
    private final String retryExceedLimit;

    /**
     * 重试限制计数提示
     */
    private final String retryCountLimit;

    LoginLimitType(String retryExceedLimit, String retryCountLimit) {
        this.retryExceedLimit = retryExceedLimit;
        this.retryCountLimit = retryCountLimit;
    }

    public String getRetryExceedLimit() {
        return retryExceedLimit;
    }

    public String getRetryCountLimit() {
        return retryCountLimit;
    }
}
