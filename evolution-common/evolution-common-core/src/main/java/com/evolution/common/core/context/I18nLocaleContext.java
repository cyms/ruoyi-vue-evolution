package com.evolution.common.core.context;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Class: I18nLocaleContext
 * <p>
 * desc: 国际化
 * </p>
 * create date: 2024-03-07 13:22:20
 *
 * @author cao.yong
 */
public class I18nLocaleContext {

    private static final MessageSource messageSource = SpringContext.getBean(MessageSource.class);

    public static String message(String message, Object... args) {
        return messageSource.getMessage(message, args, LocaleContextHolder.getLocale());
    }
}
