package com.evolution.common.core.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Class: ThreadPoolProperties
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-06 12:29:51
 *
 * @author cao.yong
 */
@ConfigurationProperties(ThreadPoolProperties.PREFIX)
public class ThreadPoolProperties {
    public static final String PREFIX = "evolution.thread.pool";
    /**
     * 是否开启线程池
     */
    private boolean enabled = false;
    /**
     * 队列最大长度
     */
    private int queueCapacity = 128;
    /**
     * 线程池维护线程所允许的空闲时间
     */
    private int keepAliveSeconds = 300;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public int getQueueCapacity() {
        return queueCapacity;
    }

    public void setQueueCapacity(int queueCapacity) {
        this.queueCapacity = queueCapacity;
    }

    public int getKeepAliveSeconds() {
        return keepAliveSeconds;
    }

    public void setKeepAliveSeconds(int keepAliveSeconds) {
        this.keepAliveSeconds = keepAliveSeconds;
    }
}
