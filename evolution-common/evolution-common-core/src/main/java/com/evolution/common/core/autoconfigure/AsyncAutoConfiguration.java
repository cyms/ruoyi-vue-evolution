package com.evolution.common.core.autoconfigure;

import jakarta.annotation.Resource;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Class: AsyncAutoConfiguration
 * <p>
 * desc: 异步线程配置
 * </p>
 * create date: 2024-03-06 15:38:54
 *
 * @author cao.yong
 */
@EnableAsync(proxyTargetClass = true)
@AutoConfiguration
@AutoConfigureAfter(ThreadPoolAutoConfiguration.class)
public class AsyncAutoConfiguration implements AsyncConfigurer {

    @Resource(name = "scheduledExecutorService")
    ScheduledExecutorService scheduledExecutorService;

    @Override
    public Executor getAsyncExecutor() {
        return scheduledExecutorService;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return AsyncConfigurer.super.getAsyncUncaughtExceptionHandler();
    }
}
