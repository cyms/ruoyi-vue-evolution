package com.evolution.common.core.autoconfigure;

import com.evolution.common.core.autoconfigure.properties.EvolutionProperties;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Class: EvolutionAutoConfiguration
 * <p>
 * desc: 自动配置类, 注解 EnableAspectJAutoProxy 开启AOP自动代理且通过AopContext对象可以访问
 * </p>
 * create date: 2024-03-06 10:36:37
 *
 * @author cao.yong
 */
@AutoConfiguration
@EnableAspectJAutoProxy(exposeProxy = true)
@EnableConfigurationProperties(EvolutionProperties.class)
public class EvolutionAutoConfiguration {
}
