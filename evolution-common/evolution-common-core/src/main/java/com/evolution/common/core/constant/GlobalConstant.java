package com.evolution.common.core.constant;

/**
 * Class: GlobalConstant
 * <p>
 * desc: 全局常量
 * </p>
 * create date: 2024-03-06 16:17:43
 *
 * @author cao.yong
 */
public interface GlobalConstant {
    /**
     * 全局 redis key (业务无关的key)
     */
    String GLOBAL_REDIS_KEY = "global:";
    /**
     * UTF-8 字符集
     */
    String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    String GBK = "GBK";

    /**
     * 防重提交 redis key
     */
    String REPEAT_SUBMIT_KEY = GLOBAL_REDIS_KEY + "resubmit:";
    /**
     * 根标识为 0
     */
    Long ROOT_ID = 0L;

    Long SUPER_ADMIN_ID = 1L;

    String TENANT_ADMIN_ROLE_KEY = "TENANT_ADMIN";
}
