package com.evolution.common.core.page;

import cn.hutool.core.convert.Convert;
import com.evolution.common.core.utils.ServletUtils;

/**
 * Class: PageSupport
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-11 18:27:08
 *
 * @author cao.yong
 */
public class PageSupport {

    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";

    /**
     * 排序列
     */
    public static final String ORDER_BY_COLUMN = "orderByColumn";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String IS_ASC = "isAsc";

    /**
     * 分页参数合理化
     */
    public static final String REASONABLE = "reasonable";

    /**
     * 封装分页对象
     */
    public static PageQuery getPageQuery() {
        PageQuery query = new PageQuery();
        query.setPageNum(Convert.toInt(ServletUtils.getParameter(PAGE_NUM), 1));
        query.setPageSize(Convert.toInt(ServletUtils.getParameter(PAGE_SIZE), 10));
        query.setOrderByColumn(ServletUtils.getParameter(ORDER_BY_COLUMN));
        query.setIsAsc(ServletUtils.getParameter(IS_ASC));
        query.setReasonable(ServletUtils.getParameterToBool(REASONABLE));
        return query;
    }

    public static PageQuery buildPageRequest() {
        return getPageQuery();
    }
}
