package com.evolution.common.core.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Class: EvolutionProperties
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-06 11:58:44
 *
 * @author cao.yong
 */
@ConfigurationProperties(EvolutionProperties.PREFIX)
public class EvolutionProperties {
    public static final String PREFIX = "evolution.app";

    private String name;

    private String version;

    private String copyright;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }
}
