package com.evolution.common.core.enums;

/**
 * Class: LoginTerminalType
 * <p>
 * desc: 登录端点类型
 * </p>
 * create date: 2024-03-06 16:04:34
 *
 * @author cao.yong
 */
public enum LoginTerminalType {
    /**
     * PC Web
     */
    PC,
    /**
     * 应用程序
     */
    APP,
    /**
     * 小程序
     */
    MINI_APP,
    /**
     * 第三方
     */
    SOCIAL
}
