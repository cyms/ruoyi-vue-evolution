package com.evolution.common.core.autoconfigure;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * Class: ValidatorAutoConfiguration
 * <p>
 * desc: 验证器配置
 * </p>
 * create date: 2024-03-06 10:58:28
 *
 * @author cao.yong
 */
@AutoConfiguration
@AutoConfigureAfter(ValidationAutoConfiguration.class)
public class ValidatorAutoConfiguration {

    @Bean
    @ConditionalOnBean(LocalValidatorFactoryBean.class)
    public Validator validator(MessageSource messageSource, LocalValidatorFactoryBean factoryBean) {
        factoryBean.setValidationMessageSource(messageSource);
        return factoryBean;
    }
}
