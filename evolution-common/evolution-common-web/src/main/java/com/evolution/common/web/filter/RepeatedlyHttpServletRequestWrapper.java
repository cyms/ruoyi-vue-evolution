package com.evolution.common.web.filter;

import cn.hutool.core.io.IoUtil;
import com.evolution.common.core.constant.GlobalConstant;
import jakarta.servlet.ReadListener;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class: RepeatedlyHttpServletRequestWrapper
 * <p>
 * desc: 可重复读取inputStream的request
 * </p>
 * create date: 2024-03-13 11:50:05
 *
 * @author cao.yong
 */
public class RepeatedlyHttpServletRequestWrapper extends HttpServletRequestWrapper {
    private final byte[] body;

    public RepeatedlyHttpServletRequestWrapper(HttpServletRequest request, ServletResponse response) throws IOException {
        super(request);
        request.setCharacterEncoding(GlobalConstant.UTF8);
        response.setCharacterEncoding(GlobalConstant.UTF8);

        body = IoUtil.readBytes(request.getInputStream(), false);
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(getInputStream()));
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        final ByteArrayInputStream bis = new ByteArrayInputStream(body);
        return new ServletInputStream() {
            @Override
            public int read() throws IOException {
                return bis.read();
            }

            @Override
            public int available() throws IOException {
                return body.length;
            }

            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener readListener) {

            }
        };
    }
}
