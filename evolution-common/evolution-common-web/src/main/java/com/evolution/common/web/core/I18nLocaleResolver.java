package com.evolution.common.web.core;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.lang.NonNull;
import org.springframework.web.servlet.LocaleResolver;

import java.util.Locale;

/**
 * Class: I18nLocaleResolver
 * <p>
 * desc: 通过请求对象获取国际化语言设置
 * </p>
 * create date: 2024-03-11 18:43:10
 *
 * @author cao.yong
 */
public class I18nLocaleResolver implements LocaleResolver {

    @NonNull
    @Override
    public Locale resolveLocale(@NonNull HttpServletRequest httpServletRequest) {
        String language = httpServletRequest.getHeader("content-language");
        Locale locale = Locale.getDefault();
        if (language != null && !language.isEmpty()) {
            String[] split = language.split("_");
            locale = new Locale(split[0], split[1]);
        }
        return locale;
    }

    @Override
    public void setLocale(@NonNull HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Locale locale) {

    }
}
