package com.evolution.common.web.core;

import com.evolution.common.core.result.R;
import com.evolution.common.core.utils.StringUtils;

/**
 * Class: BaseController
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-11 18:22:57
 *
 * @author cao.yong
 */
public class BaseController {
    /**
     * 响应返回结果
     *
     * @param rows 影响行数
     * @return 操作结果
     */
    protected R<Void> toAjax(int rows) {
        return rows > 0 ? R.ok() : R.fail();
    }

    /**
     * 响应返回结果
     *
     * @param result 结果
     * @return 操作结果
     */
    protected R<Void> toAjax(boolean result) {
        return result ? R.ok() : R.fail();
    }

    /**
     * 页面跳转
     */
    public String redirect(String url) {
        return StringUtils.format("redirect:{}", url);
    }
}
