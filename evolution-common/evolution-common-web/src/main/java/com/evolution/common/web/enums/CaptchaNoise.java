package com.evolution.common.web.enums;

import cn.hutool.captcha.AbstractCaptcha;
import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.captcha.generator.CodeGenerator;
import cn.hutool.captcha.generator.RandomGenerator;
import com.evolution.common.web.utils.UnsignedMathGenerator;

/**
 * Class: CaptchaNoise
 * <p>
 * desc: 验证码干扰类型
 * </p>
 * create date: 2024-03-11 18:46:43
 *
 * @author cao.yong
 */
public enum CaptchaNoise {
    /**
     * 线段干扰
     */
    LINE(LineCaptcha.class),

    /**
     * 圆圈干扰
     */
    CIRCLE(CircleCaptcha.class),

    /**
     * 扭曲干扰
     */
    SHEAR(ShearCaptcha.class);

    private final Class<? extends AbstractCaptcha> clazz;

    CaptchaNoise(Class<? extends AbstractCaptcha> clazz) {
        this.clazz = clazz;
    }

    public Class<? extends AbstractCaptcha> getClazz() {
        return clazz;
    }
}
