package com.evolution.common.web.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Class: XssProperties
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-13 11:42:02
 *
 * @author cao.yong
 */
@ConfigurationProperties(XssProperties.PREFIX)
public class XssProperties {
    public static final String PREFIX = "evolution.xss";

    /**
     * 过滤开关
     */
    private String enabled;

    /**
     * 排除链接（多个用逗号分隔）
     */
    private String excludes;

    /**
     * 匹配链接
     */
    private String urlPatterns;

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getExcludes() {
        return excludes;
    }

    public void setExcludes(String excludes) {
        this.excludes = excludes;
    }

    public String getUrlPatterns() {
        return urlPatterns;
    }

    public void setUrlPatterns(String urlPatterns) {
        this.urlPatterns = urlPatterns;
    }
}
