package com.evolution.common.web.autoconfigure.properties;

import com.evolution.common.web.enums.CaptchaNoise;
import com.evolution.common.web.enums.CaptchaType;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Class: CaptchaProperties
 * <p>
 * desc: 验证码配置属性
 * </p>
 * create date: 2024-03-13 11:37:36
 *
 * @author cao.yong
 */
@ConfigurationProperties(CaptchaProperties.PREFIX)
public class CaptchaProperties {

    public static final String PREFIX = "evolution.captcha";

    private Boolean enabled;

    /**
     * 验证码类型
     */
    private CaptchaType type;

    /**
     * 验证码干扰
     */
    private CaptchaNoise noise;

    /**
     * 数字验证码位数
     */
    private Integer numLen;

    /**
     * 字符验证码长度
     */
    private Integer charLen;

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public CaptchaType getType() {
        return type;
    }

    public void setType(CaptchaType type) {
        this.type = type;
    }

    public CaptchaNoise getNoise() {
        return noise;
    }

    public void setNoise(CaptchaNoise noise) {
        this.noise = noise;
    }

    public Integer getNumLen() {
        return numLen;
    }

    public void setNumLen(Integer numLen) {
        this.numLen = numLen;
    }

    public Integer getCharLen() {
        return charLen;
    }

    public void setCharLen(Integer charLen) {
        this.charLen = charLen;
    }
}
