package com.evolution.common.web.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * Class: Resubmit
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-11 18:09:31
 *
 * @author cao.yong
 */
@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Resubmit {
    /**
     * 间隔时间(ms)，小于此时间视为重复提交
     */
    public int interval() default 5000;

    /**
     * 时间单位，默认毫秒
     */
    TimeUnit timeUnit() default TimeUnit.MILLISECONDS;

    /**
     * 提示消息 支持国际化 格式为 {code}
     */
    String message() default "{repeat.submit.message}";
}
