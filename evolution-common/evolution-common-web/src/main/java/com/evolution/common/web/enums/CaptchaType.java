package com.evolution.common.web.enums;

import cn.hutool.captcha.generator.CodeGenerator;
import cn.hutool.captcha.generator.RandomGenerator;
import com.evolution.common.web.utils.UnsignedMathGenerator;

/**
 * Class: CaptchaType
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-13 11:34:41
 *
 * @author cao.yong
 */
public enum CaptchaType {

    /**
     * 数字
     */
    MATH(UnsignedMathGenerator.class),

    /**
     * 字符
     */
    CHAR(RandomGenerator.class);

    private final Class<? extends CodeGenerator> clazz;

    CaptchaType(Class<? extends CodeGenerator> clazz) {
        this.clazz = clazz;
    }

    public Class<? extends CodeGenerator> getClazz() {
        return clazz;
    }
}
