package com.evolution.common.web.autoconfigure;

import com.evolution.common.web.aspect.ResubmitAspect;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConfiguration;

/**
 * Class: ResubmitAutoConfiguration
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-13 12:26:43
 *
 * @author cao.yong
 */
@AutoConfiguration(after = RedisConfiguration.class)
public class ResubmitAutoConfiguration {

    @Bean
    public ResubmitAspect resubmitAspect() {
        return new ResubmitAspect();
    }
}
