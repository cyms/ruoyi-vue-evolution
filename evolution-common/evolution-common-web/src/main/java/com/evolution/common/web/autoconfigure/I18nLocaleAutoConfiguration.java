package com.evolution.common.web.autoconfigure;

import com.evolution.common.web.core.I18nLocaleResolver;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.LocaleResolver;

/**
 * Class: I18nLocaleAutoConfiguration
 * <p>
 * desc: 国际化自动配置
 * </p>
 * create date: 2024-03-13 12:25:06
 *
 * @author cao.yong
 */
@AutoConfiguration(before = WebMvcAutoConfiguration.class)
public class I18nLocaleAutoConfiguration {
    @Bean
    public LocaleResolver localeResolver() {
        return new I18nLocaleResolver();
    }
}
