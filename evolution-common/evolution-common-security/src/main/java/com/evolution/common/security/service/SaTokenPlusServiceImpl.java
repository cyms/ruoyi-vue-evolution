package com.evolution.common.security.service;

import cn.dev33.satoken.stp.StpInterface;
import com.evolution.common.core.enums.UserType;
import com.evolution.common.security.context.LoginContext;
import com.evolution.common.security.entity.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Class: SaTokenPlusServiceImpl
 * <p>
 * desc: sa-token 权限管理实现类
 * </p>
 * create date: 2024-03-07 18:36:01
 *
 * @author cao.yong
 */
public class SaTokenPlusServiceImpl implements StpInterface {

    /**
     * 获取菜单权限列表
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        User user = LoginContext.getUser();
        UserType userType = UserType.of(user.getUserType());
        if (userType == UserType.SYS_USER) {
            return new ArrayList<>(user.getMenuPermission());
        } else if (userType == UserType.APP_USER) {
            // 其他端 自行根据业务编写
        }
        return new ArrayList<>();
    }

    /**
     * 获取角色权限列表
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        User user = LoginContext.getUser();
        UserType userType = UserType.of(user.getUserType());
        if (userType == UserType.SYS_USER) {
            return new ArrayList<>(user.getRolePermission());
        } else if (userType == UserType.APP_USER) {
            // 其他端 自行根据业务编写
        }
        return new ArrayList<>();
    }
}
