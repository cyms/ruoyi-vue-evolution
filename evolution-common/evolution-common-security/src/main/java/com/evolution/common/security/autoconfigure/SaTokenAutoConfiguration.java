package com.evolution.common.security.autoconfigure;

import cn.dev33.satoken.dao.SaTokenDao;
import cn.dev33.satoken.jwt.StpLogicJwtForSimple;
import cn.dev33.satoken.stp.StpInterface;
import cn.dev33.satoken.stp.StpLogic;
import com.evolution.common.core.factory.YamlPropertySourceFactory;
import com.evolution.common.security.dao.SaTokenPlusDao;
import com.evolution.common.security.service.SaTokenPlusServiceImpl;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

/**
 * Class: SaTokenAutoConfiguration
 * <p>
 * desc: SaToken 自动配置
 * </p>
 * create date: 2024-03-07 18:39:45
 *
 * @author cao.yong
 */
@AutoConfiguration
@PropertySource(value = "classpath:sa-token.yml", factory = YamlPropertySourceFactory.class)
public class SaTokenAutoConfiguration {

    @Bean
    public StpLogic stpLogic() {
        return new StpLogicJwtForSimple();
    }

    @Bean
    public SaTokenDao saTokenDao() {
        return new SaTokenPlusDao();
    }

    @Bean
    public StpInterface stpInterface() {
        return new SaTokenPlusServiceImpl();
    }
}
