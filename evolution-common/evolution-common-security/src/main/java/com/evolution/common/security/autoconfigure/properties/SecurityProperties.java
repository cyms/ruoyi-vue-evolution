package com.evolution.common.security.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Class: SecurityProperties
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-11 11:33:23
 *
 * @author cao.yong
 */
@ConfigurationProperties(prefix = SecurityProperties.PREFIX)
public class SecurityProperties {
    protected static final String PREFIX = "spring.security";

    /**
     * 排除路径
     */
    private String[] excludes;

    public String[] getExcludes() {
        return excludes;
    }

    public void setExcludes(String[] excludes) {
        this.excludes = excludes;
    }
}
