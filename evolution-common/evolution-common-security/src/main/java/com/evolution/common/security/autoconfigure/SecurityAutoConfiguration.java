package com.evolution.common.security.autoconfigure;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import com.evolution.common.core.context.SpringContext;
import com.evolution.common.core.utils.ServletUtils;
import com.evolution.common.core.utils.StringUtils;
import com.evolution.common.security.autoconfigure.properties.SecurityProperties;
import com.evolution.common.security.context.LoginContext;
import com.evolution.common.security.handler.AllUrlHandler;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Class: SecurityAutoConfiguration
 * <p>
 * desc: Spring security framework auto configuration.
 * </p>
 * create date: 2024-03-11 11:31:38
 *
 * @author cao.yong
 */
@AutoConfiguration
@EnableConfigurationProperties(SecurityProperties.class)
public class SecurityAutoConfiguration implements WebMvcConfigurer {
    private final SecurityProperties securityProperties;

    public SecurityAutoConfiguration(SecurityProperties securityProperties) {
        this.securityProperties = securityProperties;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册路由拦截器，自定义验证规则
        registry.addInterceptor(new SaInterceptor(handler -> {
                    AllUrlHandler allUrlHandler = SpringContext.getBean(AllUrlHandler.class);
                    // 登录验证 -- 排除多个路径
                    SaRouter
                            // 获取所有的
                            .match(allUrlHandler.getUrls())  // 拦截的 path 列表
                            .check(() -> {
                                // 检查是否登录 是否有token
                                StpUtil.checkLogin();

                                // 检查 header 与 param 里的 clientId 与 token 里的是否一致
                                String headerCid = ServletUtils.getRequest().getHeader(LoginContext.CLIENT_ID_KEY);
                                String paramCid = ServletUtils.getParameter(LoginContext.CLIENT_ID_KEY);
                                String clientId = StpUtil.getExtra(LoginContext.CLIENT_ID_KEY).toString();
                                if (!StringUtils.equalsAny(clientId, headerCid, paramCid)) {
                                    // token 无效
                                    throw NotLoginException.newInstance(StpUtil.getLoginType(),
                                            "-100", "客户端ID与Token不匹配",
                                            StpUtil.getTokenValue());
                                }

                            });
                })).addPathPatterns("/**")
                // 排除不需要拦截的路径
                .excludePathPatterns(securityProperties.getExcludes());
    }
}
