package com.evolution.common.excel.core;

/**
 * Class: RepeatCell
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-11 15:45:48
 *
 * @author cao.yong
 */
public class RepeatCell {

    private Object value;

    private int current;

    public RepeatCell(Object value, int current) {
        this.value = value;
        this.current = current;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }
}
