package com.evolution.common.excel.annotation;

import java.lang.annotation.*;

/**
 * Class: ExcelDataConverter
 * <p>
 * desc: 字典格式化
 * </p>
 * create date: 2024-03-11 15:08:47
 *
 * @author cao.yong
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ExcelDataConverter {

    /**
     * 字典类型
     */
    String type();

    Class<?> converter();
}
