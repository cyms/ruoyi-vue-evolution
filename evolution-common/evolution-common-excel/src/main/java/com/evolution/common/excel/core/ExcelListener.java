package com.evolution.common.excel.core;


import com.alibaba.excel.read.listener.ReadListener;

/**
 * Class: ExcelListener
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-11 15:30:23
 *
 * @author cao.yong
 */
public interface ExcelListener<T> extends ReadListener<T> {

    ExcelResult<T> getExcelResult();
}
