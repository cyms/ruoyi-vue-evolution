package com.evolution.common.excel.core;

import java.util.List;

/**
 * Class: ExcelResult
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-11 15:29:34
 *
 * @author cao.yong
 */
public interface ExcelResult<T> {
    /**
     * 对象列表
     */
    List<T> getList();

    /**
     * 错误列表
     */
    List<String> getErrorList();

    /**
     * 导入回执
     */
    String getAnalysis();
}
