package com.evolution.common.excel.annotation;

import java.lang.annotation.*;

/**
 * Class: ExcelCellMerge
 * <p>
 * desc: Excel Column Merge
 * </p>
 * create date: 2024-03-11 15:07:05
 *
 * @author cao.yong
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ExcelCellMerge {

    /**
     * column index
     */
    int index() default -1;
}
