package com.evolution.common.excel.core;

import com.evolution.common.core.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Class: DefaultExcelResult
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-11 15:32:38
 *
 * @author cao.yong
 */
public class DefaultExcelResult<T> implements ExcelResult<T> {

    /**
     * 数据对象list
     */
    private List<T> list;

    /**
     * 错误信息列表
     */
    private List<String> errorList;

    public DefaultExcelResult() {
        this.list = new ArrayList<>(0);
        this.errorList = new ArrayList<>(0);
    }

    public DefaultExcelResult(List<T> list, List<String> errorList) {
        this.list = list;
        this.errorList = errorList;
    }

    public DefaultExcelResult(ExcelResult<T> result) {
        this.list = result.getList();
        this.errorList = result.getErrorList();
    }

    @Override
    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    @Override
    public List<String> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<String> errorList) {
        this.errorList = errorList;
    }

    @Override
    public String getAnalysis() {
        int successCount = list.size();
        int errorCount = errorList.size();
        if (successCount == 0) {
            return "读取失败，未解析到数据";
        } else {
            if (errorCount == 0) {
                return StringUtils.format("恭喜您，全部读取成功！共{}条", successCount);
            } else {
                return "";
            }
        }
    }
}
