package com.evolution.common.mybatis.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Class: MyBatisFlexExtendProperties
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-07 12:20:46
 *
 * @author cao.yong
 */
@ConfigurationProperties(prefix = MyBatisFlexExtendProperties.PREFIX)
public class MyBatisFlexExtendProperties {
    public static final String PREFIX = "mybatis-flex";

    /**
     * 是否打印显示SQL
     */
    private boolean showSql = false;

    /**
     * 开启审计功能
     */
    private boolean auditEnabled = false;

    public boolean isShowSql() {
        return showSql;
    }

    public void setShowSql(boolean showSql) {
        this.showSql = showSql;
    }

    public boolean isAuditEnabled() {
        return auditEnabled;
    }

    public void setAuditEnabled(boolean auditEnabled) {
        this.auditEnabled = auditEnabled;
    }
}
