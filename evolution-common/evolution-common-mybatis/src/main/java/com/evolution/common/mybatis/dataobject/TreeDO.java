package com.evolution.common.mybatis.dataobject;

import com.mybatisflex.annotation.Column;

import java.util.ArrayList;
import java.util.List;

/**
 * Class: TreeDO
 * <p>
 * desc: 树形数据结构
 * </p>
 * create date: 2024-03-07 12:58:57
 *
 * @author cao.yong
 */
public class TreeDO<T> extends BaseDO {

    /**
     * 父节点ID
     */
    private Long parentId;

    /**
     * 父节点名称
     */
    @Column(ignore = true)
    private String parentName;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 祖级列表
     */
    @Column(ignore = true)
    private String ancestors;

    /**
     * 子节点集合
     */
    @Column(ignore = true)
    private List<T> children = new ArrayList<>();

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public String getAncestors() {
        return ancestors;
    }

    public void setAncestors(String ancestors) {
        this.ancestors = ancestors;
    }

    public List<T> getChildren() {
        return children;
    }

    public void setChildren(List<T> children) {
        this.children = children;
    }
}
