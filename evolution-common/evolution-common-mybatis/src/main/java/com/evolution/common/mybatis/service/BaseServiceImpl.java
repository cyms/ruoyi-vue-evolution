package com.evolution.common.mybatis.service;

import com.evolution.common.mybatis.dataobject.BasicDO;
import com.mybatisflex.core.BaseMapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;

/**
 * Class: BaseServiceImpl
 * <p>
 * desc: 基础服务实现类
 * </p>
 * create date: 2024-03-07 14:00:54
 *
 * @author cao.yong
 */
public class BaseServiceImpl<T extends BasicDO> extends ServiceImpl<BaseMapper<T>, T> implements BaseService<T> {
}
