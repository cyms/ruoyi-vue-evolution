package com.evolution.common.mybatis.decipher;

import com.mybatisflex.core.datasource.DataSourceDecipher;
import com.mybatisflex.core.datasource.DataSourceProperty;

/**
 * Class: CustomDataSourceDecipher
 * <p>
 * desc: 自定义数据源加密算法
 * </p>
 * create date: 2024-03-07 12:47:53
 *
 * @author cao.yong
 */
public class CustomDataSourceDecipher implements DataSourceDecipher {
    @Override
    public String decrypt(DataSourceProperty property, String value) {
        //解密数据源URL、用户名、密码，通过编码支持任意加密方式的解密
        //为了减轻入门用户负担，默认返回原字符，用户可以定制加解密算法
        String result = "";
        switch (property) {
            case URL, PASSWORD, USERNAME -> result = value;
        }
        return result;
    }
}
