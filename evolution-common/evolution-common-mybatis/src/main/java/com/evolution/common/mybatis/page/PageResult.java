package com.evolution.common.mybatis.page;

import com.evolution.common.core.enums.HttpStatus;
import com.evolution.common.core.result.R;
import com.mybatisflex.core.paginate.Page;

import java.util.List;

/**
 * Class: PageResult
 * <p>
 * desc: 分页结果
 * </p>
 * create date: 2024-03-11 18:32:10
 *
 * @author cao.yong
 */
public class PageResult<T> extends R<List<T>> {

    private long total;

    public PageResult() {
    }

    /**
     * 分页
     *
     * @param list  列表数据
     * @param total 总记录数
     */
    public PageResult(List<T> list, int total) {
        this.setData(list);
        this.total = total;
    }

    public static <T> PageResult<T> build(List<T> list) {
        PageResult<T> result = new PageResult<>();
        result.setCode(HttpStatus.SUCCESS.getCode());
        result.setMsg("查询成功");
        result.setData(list);
        result.setTotal(list.size());
        return result;
    }

    public static <T> PageResult<T> build() {
        PageResult<T> result = new PageResult<>();
        result.setCode(HttpStatus.SUCCESS.getCode());
        result.setMsg("查询成功");
        return result;
    }

    public static <T> PageResult<T> build(Page<T> page) {
        PageResult<T> result = new PageResult<>();
        result.setCode(HttpStatus.SUCCESS.getCode());
        result.setMsg("查询成功");
        result.setData(page.getRecords());
        result.setTotal(page.getTotalRow());
        return result;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }
}
