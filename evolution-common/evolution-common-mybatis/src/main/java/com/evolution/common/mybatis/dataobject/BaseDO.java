package com.evolution.common.mybatis.dataobject;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Class: BaseDO
 * <p>
 * desc: 基础标准数据对象
 * </p>
 * create date: 2024-03-07 11:04:51
 *
 * @author cao.yong
 */
public class BaseDO extends BasicDO {

    private Long createBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private Long updateBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public LocalDateTime getCreateTime() {
        return null == createTime ? null : LocalDateTime.ofInstant(createTime.toInstant(), ZoneId.systemDefault());
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = Date.from(createTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public LocalDateTime getUpdateTime() {
        return null == updateTime ? null : LocalDateTime.ofInstant(updateTime.toInstant(), ZoneId.systemDefault());
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = Date.from(updateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
