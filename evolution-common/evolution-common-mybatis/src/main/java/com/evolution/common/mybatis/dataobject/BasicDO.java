package com.evolution.common.mybatis.dataobject;

import java.io.Serial;
import java.io.Serializable;

/**
 * Class: BasicDO
 * <p>
 * desc: 基本数据对象
 * </p>
 * create date: 2024-03-07 12:59:55
 *
 * @author cao.yong
 */
public class BasicDO implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
}
