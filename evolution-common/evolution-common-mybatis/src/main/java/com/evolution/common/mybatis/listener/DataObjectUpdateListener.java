package com.evolution.common.mybatis.listener;

import com.evolution.common.core.enums.HttpStatus;
import com.evolution.common.core.exception.ServiceException;
import com.evolution.common.mybatis.dataobject.BaseDO;
import com.mybatisflex.annotation.UpdateListener;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Class: DataObjectUpdateListener
 * <p>
 * desc:
 * </p>
 * create date: 2024-03-07 13:39:38
 *
 * @author cao.yong
 */
public class DataObjectUpdateListener implements UpdateListener {
    @Override
    public void onUpdate(Object entity) {
        try {
            if (Objects.nonNull(entity) && entity instanceof BaseDO baseDO) {
                baseDO.setUpdateTime(LocalDateTime.now());
            }
        } catch (Exception e) {
            throw new ServiceException(HttpStatus.UNAUTHORIZED.getCode(), "全局插入数据监听器注入异常 => " + e.getMessage());
        }
    }
}
