package com.evolution.common.mybatis.service;

import com.evolution.common.mybatis.dataobject.BasicDO;
import com.mybatisflex.core.service.IService;

/**
 * Class: BaseService
 * <p>
 * desc: 基础服务层
 * </p>
 * create date: 2024-03-07 14:00:37
 *
 * @author cao.yong
 */
public interface BaseService<T extends BasicDO> extends IService<T> {
}
