package com.evolution.common.redis.autoconfigure.properties;

/**
 * Class: SingleServerConfig
 * <p>
 * desc: 单机服务器配置
 * </p>
 * create date: 2024-03-07 17:15:04
 *
 * @author cao.yong
 */
public class SingleServerConfig {
    /**
     * 客户端名称
     */
    private String clientName;

    /**
     * 最小空闲连接数
     */
    private int connectionMinimumIdleSize;

    /**
     * 连接池大小
     */
    private int connectionPoolSize;

    /**
     * 连接空闲超时，单位：毫秒
     */
    private int idleConnectionTimeout;

    /**
     * 命令等待超时，单位：毫秒
     */
    private int timeout;

    /**
     * 发布和订阅连接池大小
     */
    private int subscriptionConnectionPoolSize;

    public SingleServerConfig() {
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public int getConnectionMinimumIdleSize() {
        return connectionMinimumIdleSize;
    }

    public void setConnectionMinimumIdleSize(int connectionMinimumIdleSize) {
        this.connectionMinimumIdleSize = connectionMinimumIdleSize;
    }

    public int getConnectionPoolSize() {
        return connectionPoolSize;
    }

    public void setConnectionPoolSize(int connectionPoolSize) {
        this.connectionPoolSize = connectionPoolSize;
    }

    public int getIdleConnectionTimeout() {
        return idleConnectionTimeout;
    }

    public void setIdleConnectionTimeout(int idleConnectionTimeout) {
        this.idleConnectionTimeout = idleConnectionTimeout;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getSubscriptionConnectionPoolSize() {
        return subscriptionConnectionPoolSize;
    }

    public void setSubscriptionConnectionPoolSize(int subscriptionConnectionPoolSize) {
        this.subscriptionConnectionPoolSize = subscriptionConnectionPoolSize;
    }
}
